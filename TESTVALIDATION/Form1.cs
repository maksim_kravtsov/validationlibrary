﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ValidationLibrary.Preparation;
namespace TESTVALIDATION
{
    public partial class Form1 : Form
    {
        private int CountWorkThreadValidator = 0;
        private int CountQueue = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private ValidationLibrary.Preparation.TakeInfo takeInfo;
        private ValidationLibrary.ValidationV2.ThreadsController Controller;

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LSchema.Text = openFileDialog1.FileName;
            }
        }

        private void BXML_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LXML.Text = openFileDialog1.FileName;
            }
        }
        DateTime start, end;
        private void button1_Click(object sender, EventArgs e)
        {
            ElementName.Enabled = false;
            BXML.Enabled = false;
            BSchema.Enabled = false;
            button1.Enabled = false;
            start = DateTime.Now;
            takeInfo = new TakeInfo(new ValidationLibrary.Objects.PreValidationParameters()
            {
                PathToXml = LXML.Text,
                Schema = new ValidationLibrary.Objects.SchemaParameters()
                {
                    Path = LSchema.Text,
                    ElementName = this.ElementName.Text
                },
                MaxCount=128000
            });
            end = DateTime.Now;
            CountQueue = takeInfo.QueueValidation.Count;
            LFirstAge.Text = String.Format("Подготовлена очередь из {0} задач за {1} секунд.", CountQueue, (end - start).TotalSeconds.ToString().Split(',')[0]);
            lFileInfo.Text = String.Format("Размер файла: {0} Мб. ",(new FileInfo(takeInfo._info.PathToXML).Length/1024)/1024);
        }

        private void BSecondAge_Click(object sender, EventArgs e)
        {
            BSecondAge.Enabled = false;
            lFileInfo.Text += "Начат поцесс валидации.";
            start = DateTime.Now;
            Controller = new ValidationLibrary.ValidationV2.ThreadsController(takeInfo.QueueValidation, takeInfo._info);
            Controller._validationBlocks.ErrorValidation += _validationBlocks_ErrorValidation;
            Controller._validationBlocks.StartValidator += _validationBlocks_StartValidator;
            Controller._validationBlocks.ValidateConpleted += _validationBlocks_ValidateConpleted;
        }

        void _validationBlocks_ValidateConpleted(object sender, EventArgs e)
        {
            CountWorkThreadValidator--;
            
            try
            {
                if (this.InvokeRequired)
                    BeginInvoke(new MethodInvoker(delegate
                    {
                        rtValid.Text += String.Format("В очереди: {0} \n", CountQueue);
                    }));
                else
                    rtValid.Text += String.Format("В очереди: {0} \n", CountQueue);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (takeInfo.QueueValidation.Count == 0)
            {
                end = DateTime.Now;
                try
                {
                    if (this.InvokeRequired)
                        BeginInvoke(new MethodInvoker(delegate
                        {
                            rtValid.Text += String.Format("Валидация завершена. Затрачено времени: {0} \n", (end - start).ToString());
                        }));
                    else
                        rtValid.Text += String.Format("Валидация завершена. Затрачено времени: {0} \n", (end - start).ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        void _validationBlocks_StartValidator(object sender, EventArgs e)
        {
            CountQueue--;
            CountWorkThreadValidator++;
            
                
                //MessageBox.Show("Валидация завершена. Затрачено времени: " + (end - start).ToString());
        }

        void _validationBlocks_ErrorValidation(object sender, System.Xml.Schema.ValidationEventArgs e)
        {
            CountWorkThreadValidator--;
            end = DateTime.Now;
            try
            {
                if (this.InvokeRequired)
                    BeginInvoke(new MethodInvoker(delegate
                    {
                        rtValid.Text += String.Format("Обнаружена ошибка в XML-документе: {0} \n", e.Message);
                    }));
                else
                    lFileInfo.Text = String.Format("Обнаружена ошибка в XML-документе: {0} \n", (end - start).ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
