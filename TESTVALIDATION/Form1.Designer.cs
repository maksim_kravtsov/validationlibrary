﻿namespace TESTVALIDATION
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.BXML = new System.Windows.Forms.Button();
            this.LXML = new System.Windows.Forms.Label();
            this.BSchema = new System.Windows.Forms.Button();
            this.LSchema = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.LFirstAge = new System.Windows.Forms.Label();
            this.ElementName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BSecondAge = new System.Windows.Forms.Button();
            this.lFileInfo = new System.Windows.Forms.Label();
            this.rtValid = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // BXML
            // 
            this.BXML.Location = new System.Drawing.Point(26, 22);
            this.BXML.Name = "BXML";
            this.BXML.Size = new System.Drawing.Size(75, 20);
            this.BXML.TabIndex = 1;
            this.BXML.Text = "XML";
            this.BXML.UseVisualStyleBackColor = true;
            this.BXML.Click += new System.EventHandler(this.BXML_Click);
            // 
            // LXML
            // 
            this.LXML.AutoSize = true;
            this.LXML.Location = new System.Drawing.Point(107, 26);
            this.LXML.Name = "LXML";
            this.LXML.Size = new System.Drawing.Size(68, 13);
            this.LXML.TabIndex = 2;
            this.LXML.Text = "G:\\DB22.xml";
            // 
            // BSchema
            // 
            this.BSchema.Location = new System.Drawing.Point(26, 48);
            this.BSchema.Name = "BSchema";
            this.BSchema.Size = new System.Drawing.Size(75, 20);
            this.BSchema.TabIndex = 3;
            this.BSchema.Text = "Schema";
            this.BSchema.UseVisualStyleBackColor = true;
            this.BSchema.Click += new System.EventHandler(this.button2_Click);
            // 
            // LSchema
            // 
            this.LSchema.AutoSize = true;
            this.LSchema.Location = new System.Drawing.Point(107, 52);
            this.LSchema.Name = "LSchema";
            this.LSchema.Size = new System.Drawing.Size(53, 13);
            this.LSchema.TabIndex = 4;
            this.LSchema.Text = "D:\\xsd.txt";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(26, 103);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 22);
            this.button1.TabIndex = 5;
            this.button1.Text = "Собрать информацию";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LFirstAge
            // 
            this.LFirstAge.AutoSize = true;
            this.LFirstAge.Location = new System.Drawing.Point(209, 108);
            this.LFirstAge.Name = "LFirstAge";
            this.LFirstAge.Size = new System.Drawing.Size(51, 13);
            this.LFirstAge.TabIndex = 6;
            this.LFirstAge.Text = "LFirstAge";
            // 
            // ElementName
            // 
            this.ElementName.Location = new System.Drawing.Point(111, 77);
            this.ElementName.Name = "ElementName";
            this.ElementName.Size = new System.Drawing.Size(182, 20);
            this.ElementName.TabIndex = 7;
            this.ElementName.Text = "CollectionDetail";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Имя элемента";
            // 
            // BSecondAge
            // 
            this.BSecondAge.Location = new System.Drawing.Point(26, 131);
            this.BSecondAge.Name = "BSecondAge";
            this.BSecondAge.Size = new System.Drawing.Size(145, 23);
            this.BSecondAge.TabIndex = 9;
            this.BSecondAge.Text = "Валидация";
            this.BSecondAge.UseVisualStyleBackColor = true;
            this.BSecondAge.Click += new System.EventHandler(this.BSecondAge_Click);
            // 
            // lFileInfo
            // 
            this.lFileInfo.AutoSize = true;
            this.lFileInfo.Location = new System.Drawing.Point(209, 136);
            this.lFileInfo.Name = "lFileInfo";
            this.lFileInfo.Size = new System.Drawing.Size(35, 13);
            this.lFileInfo.TabIndex = 11;
            this.lFileInfo.Text = "label2";
            // 
            // rtValid
            // 
            this.rtValid.Location = new System.Drawing.Point(13, 182);
            this.rtValid.Name = "rtValid";
            this.rtValid.Size = new System.Drawing.Size(745, 248);
            this.rtValid.TabIndex = 12;
            this.rtValid.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 442);
            this.Controls.Add(this.rtValid);
            this.Controls.Add(this.lFileInfo);
            this.Controls.Add(this.BSecondAge);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ElementName);
            this.Controls.Add(this.LFirstAge);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.LSchema);
            this.Controls.Add(this.BSchema);
            this.Controls.Add(this.LXML);
            this.Controls.Add(this.BXML);
            this.Name = "Form1";
            this.Text = "Валидации XML-документа";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button BXML;
        private System.Windows.Forms.Label LXML;
        private System.Windows.Forms.Button BSchema;
        private System.Windows.Forms.Label LSchema;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label LFirstAge;
        private System.Windows.Forms.TextBox ElementName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BSecondAge;
        private System.Windows.Forms.Label lFileInfo;
        private System.Windows.Forms.RichTextBox rtValid;
    }
}

