﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationLibrary
{
    /// <summary>
    /// На момент 28.06.2015 почти все обьекты обьявлены как публичные, для возможности использовать обаботкичи событий.
    /// </summary>
    public class ValidationMainPoint
    {
        public Preparation.TakeInfo takeInfo;
        public ValidationV2.ThreadsController Controller;

        /// <summary>
        /// Входная точка для валидации. 
        /// </summary>
        /// <param name="pathToXML">Путь до XML-документа</param>
        /// <param name="pathToSchema">Путь до схемы документа</param>
        /// <param name="ArrayElementName">Имя элемента, узлы которого можно разбить на блоки</param>
        public ValidationMainPoint(string pathToXML, string pathToSchema, string ArrayElementName)
        {
            takeInfo = new Preparation.TakeInfo(new ValidationLibrary.Objects.PreValidationParameters()
            {
                PathToXml = pathToXML,
                Schema = new ValidationLibrary.Objects.SchemaParameters()
                {
                    Path = pathToSchema,
                    ElementName = ArrayElementName
                },
                MaxCount = 128000
            });

            Controller = new ValidationLibrary.ValidationV2.ThreadsController(takeInfo.QueueValidation, takeInfo._info);
        }
    }
}
