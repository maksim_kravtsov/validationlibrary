﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Linq;
using System.Windows.Forms;
using ValidationLibrary.Objects;

namespace ValidationLibrary.Validation
{
    public class Validation
    {

        List<TimeWork> workTime = new List<TimeWork>();

        DateTime start, end;
        /// <summary>
        /// все результаты ваполнения валидаций
        /// </summary>
        public List<ValidationTaskResult> results=new List<ValidationTaskResult>();
        /// <summary>
        /// строитель валидационных блоков
        /// </summary>
        public ValidationBlockBuilder builder;
        /// <summary>
        /// валидаторы
        /// </summary>
        private List<Thread> ThreadsValidation = new List<Thread>();
        public bool error = false;
        /// <summary>
        /// количество свободных потоков в данный момент
        /// </summary>
        public int GetValidationThreadsWaiting
        {
            get
            {
                return 5;//ThreadsValidation.Where(o => o.ThreadState == ThreadState.Unstarted | o.ThreadState == ThreadState.Suspended | o.ThreadState == ThreadState.Stopped).ToArray().Length;
            }
        }
        private int count;
        public Validation(int ThreadCount)
        {
            start = DateTime.Now;
            count = ThreadCount;
            //определение потоков
            ThreadsValidation = new List<Thread>(ThreadCount);
        }


        /// <summary>
        /// попытка задействовать свободный поток
        /// </summary>
        /// <param name="vo"></param>
        /// <returns>вернет true, в случае, если поток валидации запущен</returns>
        public bool ValidationStart(ValidationTaskData vo)
        {
            if (error) return false;
            if (ThreadsValidation.Count < count)
            {
                int index = ThreadsValidation.Count;
                ThreadsValidation.Add(new Thread(() =>
                        {
                            results.Add(ThreadValidationXSD(vo));
                        }));
                ThreadsValidation[index].Start();
                return true;
            }
            return SetValidThread(vo);
        }

        /// <summary>
        /// запуск первого свободного потока для валидации
        /// </summary>
        /// <param name="vo"></param>
        private bool SetValidThread(ValidationTaskData vo)
        {
            bool result = false;
            for (int i = 0; i < ThreadsValidation.Count;i++ )
                    if (ThreadsValidation[i].ThreadState == ThreadState.Unstarted | ThreadsValidation[i].ThreadState == ThreadState.Suspended | ThreadsValidation[i].ThreadState == ThreadState.Stopped)
                    {
                        ThreadsValidation[i] = new Thread(() =>
                        {
                            results.Add(ThreadValidationXSD(vo));
                        });
                        ThreadsValidation[i].Start();
                        result = true;
                        break;
                    }
            
            return result;
        }

        /// <summary>
        /// валидация блока
        /// </summary>
        /// <param name="obj"></param>
        public ValidationTaskResult ThreadValidationXSD(object obj)
        {
            TimeWork tf = new TimeWork("ValidationBlockBuilder.ThreadValidationXSD");
            //тут описать процесс валидации. ЭТО В ПОТОКАХ
            ValidationTaskData _obj = (ValidationTaskData)obj;
            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add(_obj.Schema);
            var res = new ValidationTaskResult();
            res.guid = _obj.guid;
            XDocument XMLDocument = XDocument.Load(new XmlTextReader(new StringReader(_obj.XML)));
            XMLDocument.Validate(schemas, (o, e) =>
            {
                res.Message = e.Message;
                res.exceptionLine = e.Exception.LineNumber;
                res.exceptionHResult = e.Exception.HResult;
                res.Type = e.Severity.ToString();
                error = true;
                end = DateTime.Now;
            });
            tf.setEnd();
            workTime.Add(tf);
            return res;
        }
    }
}
