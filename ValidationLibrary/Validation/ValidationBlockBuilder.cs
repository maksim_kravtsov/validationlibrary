﻿using System;
using System.Collections.Generic;
using ValidationLibrary.Objects;
using System.Xml.Schema;
using System.IO;
using System.Xml;
using System.Threading;

namespace ValidationLibrary.Validation
{
    public class ValidationBlockBuilder
    {

        public Validation valid;

        /// <summary>
        /// фиксация времени выполнения процесса
        /// </summary>
        public List<TimeWork> workTime = new List<TimeWork>();

        public string ValidBlock = "";

        /// <summary>
        /// строка декларации
        /// </summary>
        string _declarationLine=null;
        /// <summary>
        /// строка оригинального корня
        /// </summary>
        string _rootOriginLine = null;
        /// <summary>
        /// строка альтернативного корня
        /// </summary>
        string _rootAlternativeLine = null;
        /// <summary>
        /// имя корня
        /// </summary>
        string _rootName=null;
        /// <summary>
        /// имя альтернативного корня
        /// </summary>
        string _rootAltName = null;

        /// <summary>
        /// Начилие в очереди элементов
        /// </summary>
        public bool IsQueue
        {
            get
            {
                if (_queue.Count > 0) return true;
                else return false;
            }
        }

        /// <summary>
        /// очередь валидационных блоков
        /// </summary>
        private Queue<ValidationTaskInfo> _queue;
        /// <summary>
        /// схема для валидации
        /// </summary>
        private XmlSchema Orig, Alt;
        /// <summary>
        /// путь до XML-Документа
        /// </summary>
        private string _pathToXMLDocument;
        /// <summary>
        /// позиция корня
        /// </summary>
        private int _rootPosition;
        private int _rootAltPosition;
        /// <summary>
        /// Имя элемента, который первый встречается в альтернативном корне
        /// </summary>
        private string _parseElement = null;

        public ValidationBlockBuilder(Queue<ValidationTaskInfo> queue, string XmlPath, 
            XmlSchema OrigionalSchema, XmlSchema AlternativeSchema, int AlternativeRootPosition, 
            string AlternativeRootName, string OriginalRootName, int OriginalRootPosition,
            string ParseElementName)
        {
            _parseElement = ParseElementName;
            _queue = queue;
            _pathToXMLDocument = XmlPath;
            Orig = OrigionalSchema;
            Alt = AlternativeSchema;
            _rootPosition = OriginalRootPosition;
            _rootAltPosition = AlternativeRootPosition;
            _rootName=OriginalRootName;
            _rootAltName = AlternativeRootName;
            setValues();
            valid = new Validation(4);
            new Thread(XMLReaderThreadControl).Start();
        }

        private bool firstLine=true;
        public ValidationTaskData Get()
        {
            var t = _queue.Dequeue();
            return new ValidationTaskData(t)
            {
                Schema = Alt,
                XML = XMLReader(t)
            };
        }

        string DeclarationLine=String.Empty;
        string RootLine = String.Empty;
        int line = 0;
        private void setValues()
        {
            
            using (StreamReader _reader = new StreamReader(new FileStream(_pathToXMLDocument, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                TimeWork tf = new TimeWork("ValidationBlockBuilder.setValues");
                string lastLine = String.Empty;
                while (line < _rootAltPosition || line < _rootPosition)
                {
                    lastLine = _reader.ReadLine();
                    line++;

                    if (line == 1)
                        _declarationLine = lastLine;
                    if (line == _rootPosition)
                        _rootOriginLine = lastLine;
                    if (line == _rootAltPosition)
                        _rootAlternativeLine = lastLine;
                }
                _reader.Close();
                tf.setEnd();
                workTime.Add(tf);
            }
        }

        XmlTextReader reader = null;

        /// <summary>
        /// Чтение узлов
        /// </summary>
        /// <param name="t">задача</param>
        /// <param name="ElementName">Имя элемента, которое идет первым в списке дочерних узлов альтернативаного корня</param>
        /// <returns>вернет валидационный блок</returns>
        private string XMLReader(ValidationTaskInfo t)
        {
            TimeWork tf = new TimeWork("ValidationBlockBuilder.XMLReader");
            if (reader==null)
                reader = new XmlTextReader(_pathToXMLDocument);
            reader.WhitespaceHandling = WhitespaceHandling.None;

            string result = String.Empty;

            string bufferLine;
            if (reader.Name != _parseElement)
                reader.ReadToFollowing(_parseElement);

            while ((bufferLine = reader.ReadOuterXml()) != null )
            {
                result += bufferLine;
                if (t.EndLineNumber <= reader.LineNumber)
                    break;
            }
            tf.setEnd();
            workTime.Add(tf);
            string res = null;
            if (t.Alternative)
                res = String.Format("{0} {1} {2} </{3}>", _declarationLine, _rootAlternativeLine, result, _rootAltName);
            else
                res = String.Format("{0} {1} {2} </{3}>", _declarationLine, _rootOriginLine, result, _rootName);
            return res;
        }

        private string tempContainer = null;

        private void XMLReaderThreadControl()
        {
            while (!valid.error && _queue.Count>0)
            {
                var t = _queue.Peek();
                if (tempContainer == null)
                    tempContainer = XMLReader(t);
                if (valid.ValidationStart(new ValidationTaskData(t) { XML = tempContainer, Schema = Alt}))
                {
                    _queue.Dequeue();
                    tempContainer = null;
                }
                Thread.Sleep(500);
            }
        }

        


    }
}
