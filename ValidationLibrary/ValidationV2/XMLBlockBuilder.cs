﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using ValidationLibrary.Objects;

namespace ValidationLibrary.ValidationV2
{
    public class XMLBlockBuilder
    {
        public XmlDocumentInfo _info = null;
        XmlTextReader reader = null;

        public XMLBlockBuilder(XmlDocumentInfo info)
        {
            if (_info == null)
                _info = info;
        }

        /// <summary>
        /// возвращает обьект для валидации
        /// </summary>
        /// <returns></returns>
        public ValidationTaskData Get(ValidationTaskInfo t)
        {
            var _schema = _info.Alternative.Schema;
            if (!t.Alternative) _schema = _info.Original.Schema;

            return new ValidationTaskData(t)
            {
                Schema = _schema,
                XML = XMLReader(t)
            };
        }

        /// <summary>
        /// Чтение узлов
        /// </summary>
        /// <param name="t">задача</param>
        /// <param name="ElementName">Имя элемента, которое идет первым в списке дочерних узлов альтернативаного корня</param>
        /// <returns>вернет валидационный блок</returns>
        private string XMLReader(ValidationTaskInfo t)
        {
            string result = String.Empty;
            string buffer = null;

            string declaration = _info.DeclarationLine;

            string elementName = _info.Alternative.FirstElementName;
            string rootLine = _info.Alternative.Line;
            string rootName = _info.Alternative.NameElement;
            if (t.Alternative == false)
            {
                rootLine = _info.Original.Line;
                rootName = _info.Original.NameElement;
            }

            if (reader == null)
            {
                reader = new XmlTextReader(_info.PathToXML);
                reader.WhitespaceHandling = WhitespaceHandling.None;
            }
            if (t.Alternative == false)
            {
                using (StreamReader r = new StreamReader(_info.PathToXML))
                {
                    int k = 0;
                    string buf = String.Empty;
                    while ((buf = r.ReadLine()) != null)
                    {
                        k++;
                        if (t.StartLineNumber > k)
                            result += buf;
                        else
                            break;
                    }
                }
            }
            if (reader.Name != elementName)
                reader.ReadToFollowing(elementName);

            while ((buffer = reader.ReadOuterXml()) != null)
            {
                result += buffer;
                if (t.EndLineNumber <= reader.LineNumber)
                    break;
            }
            if (t.Alternative)
                return String.Format("{0} {1} {2} </{3}>", declaration, rootLine, result, rootName);
            else
                return String.Format("{0} </{1}> </{2}>", result, _info.Alternative.NameElement, rootName);
        }
    }
}
