﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using ValidationLibrary.Objects;

namespace ValidationLibrary.ValidationV2
{
    /// <summary>
    /// контролирует процесс валидации и построения блоков валидации ОДНОГО ФАЙЛА
    /// </summary>
    public class ThreadsController
    {
        //закончились элементы в очереди
        //взят элемент из очереди
        //

        public ValidationEventArgs _e;

        public event EventHandler ValidationComplete;

        protected virtual void OnValidationComplete(EventArgs e)
        {
            if (ValidationComplete != null)
                ValidationComplete(this, e);
        }

        /// <summary>
        /// Валидатор блоков
        /// </summary>
        public Validator _validationBlocks;

        /// <summary>
        /// Начилие в очереди элементов
        /// </summary>
        public bool IsQueue
        {
            get
            {
                if (_queue.Count > 0) return true;
                else return false;
            }
        }

        /// <summary>
        /// очередь валидационных блоков
        /// </summary>
        private Queue<ValidationTaskInfo> _queue;

        public void AddQueue(ValidationTaskInfo vti)
        {
            _queue.Enqueue(vti);
        }

        public ThreadsController(Queue<ValidationTaskInfo> queue, XmlDocumentInfo info)
        {
            builder = new XMLBlockBuilder(info);
            _queue = queue;
            _validationBlocks = new Validator();
            _validationBlocks.ErrorValidation += valid_ErrorValidation;
            new Thread(XMLReaderThreadControl).Start();
        }

        
        void valid_ErrorValidation(object sender, ValidationEventArgs e)
        {
            _e = e;
            OnValidationComplete(new EventArgs());
        }

        private ValidationTaskData tempContainer = null;

        /// <summary>
        /// Строитель 
        /// </summary>
        XMLBlockBuilder builder = null;
        private void XMLReaderThreadControl()
        {
            while (_queue.Count > 0)
            {
                var t = _queue.Peek();
                if (tempContainer == null)
                    tempContainer = builder.Get(t);
                if (_validationBlocks.StartValid(tempContainer))
                {
                    _queue.Dequeue();
                    tempContainer = null;
                }
                Thread.Sleep(200);
            }
        }
    }
}
