﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using ValidationLibrary.Objects;
namespace ValidationLibrary.ValidationV2
{
    //конец валидации
    //начало валидации

    public delegate void ValidationEventHandler(object sender, ValidationEventArgs e);
    /// <summary>
    /// Многопоточный валидатор XML-документов
    /// </summary>
    public class Validator
    {
        #region Handlers

        /// <summary>
        /// Ошибка валидации
        /// </summary>
        public event ValidationEventHandler ErrorValidation;

        /// <summary>
        /// Окончание валидации потоком
        /// </summary>
        public event EventHandler ValidateConpleted;

        public event EventHandler StartValidator;

        /// <summary>
        /// Обработчик ошибки валидации
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnErrorValidation(ValidationEventArgs e)
        {
            if (ErrorValidation != null)
                ErrorValidation(this, e);
        }

        /// <summary>
        /// Событие окончания валидации одним из потоков
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnValidateConpleted(EventArgs e)
        {
            if (ValidateConpleted != null)
                ValidateConpleted(this, e);
        }

        protected virtual void OnStartValidator(EventArgs e)
        {
            if (StartValidator != null)
                StartValidator(this, e);
        }

        #endregion
        /// <summary>
        /// Массив потоков валидации
        /// </summary>
        private List<Thread> ThreadsValidation = new List<Thread>();
        /// <summary>
        /// Максимальное количество потоков
        /// </summary>
        private int count = 4;

        /// <summary>
        /// Проверка наличия свободных потоков валидации
        /// </summary>
        public bool CheckThreads
        {
            get
            {
                if (ThreadsValidation.Count < count)
                    return true;
                for (int i = 0; i < ThreadsValidation.Count; i++)
                    if (ThreadsValidation[i].ThreadState == ThreadState.Unstarted | ThreadsValidation[i].ThreadState == ThreadState.Suspended | ThreadsValidation[i].ThreadState == ThreadState.Stopped)
                        return true;
                return false;
            }
        }

        /// <summary>
        /// Задействует первый свободный поток на валидацию входного XML-блока
        /// </summary>
        /// <param name="vo"></param>
        /// <returns>Вернет true, в случае, если поток валидации запущен</returns>
        public bool StartValid(ValidationTaskData vo)
        {
            if (ThreadsValidation.Count < count)
            {
                int index = ThreadsValidation.Count;
                ThreadsValidation.Add(new Thread(() =>
                        {
                            Validate(vo);
                        }));
                ThreadsValidation[index].Start();
                return true;
            }
            else
            {
                for (int i = 0; i < ThreadsValidation.Count; i++)
                    if (ThreadsValidation[i].ThreadState == ThreadState.Unstarted | ThreadsValidation[i].ThreadState == ThreadState.Suspended | ThreadsValidation[i].ThreadState == ThreadState.Stopped)
                    {
                        ThreadsValidation[i] = new Thread(() =>
                        {
                            Validate(vo);
                        });
                        ThreadsValidation[i].Start();
                        return true;
                    }
            }

            return false;
        }

        /// <summary>
        /// Валидатор
        /// </summary>
        /// <param name="obj"></param>
        public void Validate(object obj)
        {
            OnStartValidator(new EventArgs());
            bool error = false;
            ValidationTaskData _obj = (ValidationTaskData)obj;
            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add(_obj.Schema);
            var res = new ValidationTaskResult();
            res.guid = _obj.guid;
            XDocument XMLDocument = XDocument.Load(new XmlTextReader(new StringReader(_obj.XML)));
            XMLDocument.Validate(schemas, (o, e) =>
            {
                OnErrorValidation(e);//обработчик ошибки валидации
            });
            if (!error)
                OnValidateConpleted(new EventArgs());//обработка завершения валидации блока
        }
    }
}
