﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace ValidationLibrary.Objects
{
    /// <summary>
    /// Валидационный блок
    /// </summary>
    public class ValidationTaskData
    {
        /// <summary>
        /// задача, по которой был построен блок
        /// </summary>
        public ValidationTaskInfo TaskInfo=null;
        /// <summary>
        /// часть XML-документа, синтаксически правильная
        /// </summary>
        public string XML = null;
        /// <summary>
        /// идентификатор задачи
        /// </summary>
        public string guid
        {
            get
            {
                if (TaskInfo != null)
                    return TaskInfo.guid;
                else
                    return null;
            }
        }

        /// <summary>
        /// схема, по которой будет проверяться документ
        /// </summary>
        public XmlSchema Schema = null;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="tv">задача</param>
        public ValidationTaskData(ValidationTaskInfo tv)
        {
            TaskInfo = tv;
        }
    }
}
