﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationLibrary.Objects
{
    public class TimeWork
    {
        public DateTime Start;
        public DateTime End;
        public string ProcessName;
        /// <summary>
        /// вернет время, прошеднее с момента начала до момента окончания операции
        /// </summary>
        public TimeSpan GetTime { get { return End - Start; } }

        /// <summary>
        /// ФИКСИРУЕТ ВРЕМЯ СТАРТА.
        /// </summary>
        public TimeWork()
        {
            Start = DateTime.Now;
        }
        /// <summary>
        /// ФИКСИРУЕТ ВРЕМЯ СТАРТА.
        /// </summary>
        public TimeWork(string processName)
        {
            ProcessName = processName;
            Start = DateTime.Now;
        }
        /// <summary>
        /// зафиксировать время окончания
        /// </summary>
        public void setEnd()
        {
            End = DateTime.Now;
        }

        public override string ToString()
        {
            return GetTime.TotalSeconds.ToString();
        }
    }
}
