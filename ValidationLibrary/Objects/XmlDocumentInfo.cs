﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace ValidationLibrary.Objects
{
    public class XmlDocumentInfo
    {
        public class Info
        {
            /// <summary>
            /// Номер строки, где находится корневой элемент
            /// </summary>
            public int LineNumber;
            /// <summary>
            /// Имя корневого элемента
            /// </summary>
            public string NameElement;
            /// <summary>
            /// Схема документа
            /// </summary>
            public XmlSchema Schema;
            /// <summary>
            /// Строка корня
            /// </summary>
            public string Line;
            /// <summary>
            /// Название первого встречающегося дочернего узла
            /// </summary>
            public string FirstElementName;

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="LineNumber">Номер строки, где находится корневой элемент</param>
            /// <param name="NameElement">Имя корневого элемента</param>
            /// <param name="Schema">Схема документа</param>
            /// <param name="Line">Строка корня</param>
            /// <param name="FirstElementName">Название первого встречающегося дочернего узла</param>
            public Info(int LineNumber, string NameElement, XmlSchema Schema, string Line, string FirstElementName = null)
            {
                this.Line = Line;
                this.LineNumber = LineNumber;
                this.NameElement = NameElement;
                this.Schema = Schema;

                if (FirstElementName != null)
                    this.FirstElementName = FirstElementName;
            }

            public Info(XmlSchema Schema)
            {
                this.Schema = Schema;
            }

        }

        public XmlDocumentInfo(string PathToXML, XmlSchema Alternative, XmlSchema Original, int AlternativeRootPosition, string AlternativeRootName,
            string OriginalRootName, int OriginalRootPosition, string ParseElementName)
        {
            guid = Guid.NewGuid().ToString();
            this.PathToXML = PathToXML;
            this.Alternative = new Info(Alternative);
            this.Original = new Info(Original);
            this.Original.LineNumber = OriginalRootPosition;
            this.Alternative.LineNumber = AlternativeRootPosition;
            this.Original.NameElement = OriginalRootName;
            this.Alternative.NameElement = AlternativeRootName;
            this.Alternative.FirstElementName = ParseElementName;
            ReadInfo();
        }

        /// <summary>
        /// Идентификатор документа
        /// </summary>
        public readonly string guid = null;

        /// <summary>
        /// Строка декларации
        /// </summary>
        public string DeclarationLine = null;

        /// <summary>
        /// Путь до XML-документа
        /// </summary>
        public string PathToXML = null;

        /// <summary>
        /// Алтернативный корень
        /// </summary>
        public Info Alternative;
        /// <summary>
        /// Оригинальный корень
        /// </summary>
        public Info Original;

        /// <summary>
        /// Получение информации он документе. ПЕНЕНЕСТИ В ПЕРВЫЙ АЛГОРИТМ
        /// </summary>
        private void ReadInfo()
        {
            using (StreamReader _reader = new StreamReader(new FileStream(PathToXML, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                int line = 0;
                string lastLine = String.Empty;
                while (line < this.Alternative.LineNumber || line < this.Original.LineNumber)
                {
                    lastLine = _reader.ReadLine();
                    line++;

                    if (line == 1)
                        this.DeclarationLine = lastLine;
                    if (line == this.Original.LineNumber)
                        this.Original.Line = lastLine;
                    if (line == this.Alternative.LineNumber)
                        this.Alternative.Line = lastLine;
                }
                _reader.Close();
            }
        }

    }
}
