﻿using System;
using ValidationLibrary.Objects.Enums;
namespace ValidationLibrary.Objects
{
    /// <summary>
    /// Задача на валидацию, содержит информацию о блоке, которая необходима для построения валидационного блока
    /// </summary>
    public class ValidationTaskInfo
    {
        public readonly string guidDocument = null;
        /// <summary>
        /// нормер строки, где начинается блок
        /// </summary>
        public readonly int StartLineNumber;
        /// <summary>
        /// нормер строки, где кончается блок
        /// </summary>
        public readonly int EndLineNumber;
        /// <summary>
        /// схема построения
        /// </summary>
        public readonly bool Alternative = true;
        /// <summary>
        /// статус выполнения
        /// </summary>
        public ValidationWorkStatus Status = ValidationWorkStatus.Unstarted;
        /// <summary>
        /// идентификатор задачи
        /// </summary>
        public readonly string guid;
        /// <summary>
        /// конструктор 
        /// </summary>
        /// <param name="start">ормер строки, где начинается блок</param>
        /// <param name="end">нормер строки, где кончается блок</param>
        public ValidationTaskInfo(int start, int end, string DocumentGuid)
        {
            this.guidDocument = DocumentGuid;
            this.guid = Guid.NewGuid().ToString();
            this.StartLineNumber = start;
            this.EndLineNumber = end;
        }
        public ValidationTaskInfo(int start, int end, string DocumentGuid, bool Alternative)
        {
            this.guidDocument = DocumentGuid;
            this.guid = Guid.NewGuid().ToString();
            this.StartLineNumber = start;
            this.EndLineNumber = end;
            this.Alternative = Alternative;
        }
        /// <summary>
        /// строковое представление обьекта
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("start:{0} end:{1}", StartLineNumber, EndLineNumber);
        }
    }
}
