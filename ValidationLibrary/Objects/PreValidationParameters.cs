﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationLibrary.Objects
{
    public class PreValidationParameters
    {
        public string PathToXml;
        public SchemaParameters Schema;
        public int MaxCount = 2000;
    }
}
