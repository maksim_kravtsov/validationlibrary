﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
namespace ValidationLibrary.Objects
{
    public class ValidationTaskResult
    {
        public string guid;
        public string Message;
        public int exceptionLine;
        public int exceptionHResult;
        public string Type;
    }
}
