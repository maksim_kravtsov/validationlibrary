﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationLibrary.Objects.Enums
{
    public enum ValidationWorkStatus
    {
        Unstarted,
        Started,
        Error,
        Complered
    }
}
