﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationLibrary.Objects
{
    public class SchemaParameters
    {
        public string Path;
        public string ElementName;
    }
}
