﻿using System.Xml;

namespace ValidationLibrary.Objects
{
    public class IndexationElementInfo
    {
        public int ParentElementLineNumber;//индекс родителя
        public string Name;//имя элемента
        public int LineNumber;//позиция в файле
        public int EndLineNumber;//позиция закрывающего тэга
        public XmlNodeType Type;//тип элемента
        public bool isEmpty = false;
        public override string ToString()
        {
            return LineNumber + " " + EndLineNumber + " " + Name + " " + isEmpty.ToString();
        }
    }
}
