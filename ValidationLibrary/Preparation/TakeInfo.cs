﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ValidationLibrary.Objects;

namespace ValidationLibrary.Preparation
{
    public class TakeInfo
    {
        /// <summary>
        /// Очередь валидации
        /// </summary>
        public Queue<ValidationTaskInfo> QueueValidation = new Queue<ValidationTaskInfo>();
        /// <summary>
        /// Подготовка схемы
        /// </summary>
        public Schema schema;
        /// <summary>
        /// Индексирование XML-документа
        /// </summary>
        public Indexation _indexation;

        /// <summary>
        /// Информация о подготовленном документе
        /// </summary>
        public XmlDocumentInfo _info;

        //1. Схема
        //2. индексация
        //3. блоки и очередь
        public TakeInfo(PreValidationParameters Parameters)
        {
            //1
            schema = new Schema();
            schema.SetSchemaPath(Parameters.Schema);

            using (StreamReader _streamXML = new StreamReader(Parameters.PathToXml))
            {
                //2
                _indexation = new Indexation(_streamXML, Parameters.Schema.ElementName);
                _indexation.readElements();
                
                var array = MergerIndexes.MergeFast(ref _indexation.IndexElements, ref _indexation.EndIndexElements);

                _info = new XmlDocumentInfo(Parameters.PathToXml, schema.XSDSchemaAlternative, 
                    schema.XSDSchemaOriginal, _indexation.AlternativeRootLineNumber, 
                    _indexation.AlternativeRootName, _indexation.OriginalRootName, 
                    _indexation.OriginalRootLineNumber, array.First().Name);
                //3
                QueueValidation = Segmentation.GetQueue(array, _info.guid, Parameters.MaxCount);
            }
        }
    }
}
