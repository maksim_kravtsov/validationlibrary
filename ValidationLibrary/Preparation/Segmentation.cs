﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ValidationLibrary.Objects;
namespace ValidationLibrary.Preparation
{
    /// <summary>
    /// Выделение из документа блоков
    /// </summary>
    public static class Segmentation
    {
        /// <summary>
        /// Выделение блоков и формирование очереди
        /// </summary>
        /// <param name="IndexElements">Массив индексиов элементов с номерами строк начала узла и конца</param>
        /// <param name="DocumentInfo">Информация о документе</param>
        /// <param name="MaxCount">Максимальное количество элементов в блоке (В счет идут дочерние узлы корня)</param>
        /// <returns>Очередь</returns>
        public static Queue<ValidationTaskInfo> GetQueue(IndexationElementInfo[] IndexElements, string DocumentGuid, int MaxCount = 2000)
        {
            bool Alternative = false;
            Queue<ValidationTaskInfo> resultQueue = new Queue<ValidationTaskInfo>();
            int start = 0, end = 0, j = 0;
            for (int i = 0; i < IndexElements.Length; i++)
            {
                j++;
                if (j == 1)
                    start = IndexElements[i].LineNumber;

                if (j == MaxCount)
                {
                    end = IndexElements[i].EndLineNumber;
                    if (Alternative)
                        resultQueue.Enqueue(new ValidationTaskInfo(start, end, DocumentGuid));
                    else
                    {
                        resultQueue.Enqueue(new ValidationTaskInfo(start, end, DocumentGuid, Alternative));
                        Alternative = true;
                    }
                    j = 0;
                }

            }
            return resultQueue;
        }
    }
}
