﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationLibrary.Objects;

namespace ValidationLibrary.Preparation
{
    public static class MergerIndexes
    {
        /// <summary>
        /// LINQ Merge (точный и долгий)
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static IndexationElementInfo[] Merge(ref List<IndexationElementInfo> start, ref List<IndexationElementInfo> end)
        {
            for (int i = 0; i < start.Count; i++)
            {
                int line=start[i].ParentElementLineNumber;
                string name=start[i].Name;
                int stLine = start[i].LineNumber;
                start[i].EndLineNumber = end.Where(o =>
                    o.ParentElementLineNumber == line
                    && o.Name == name && o.LineNumber > stLine).FirstOrDefault().LineNumber;
            }

            end.Clear();
            var result = start.ToArray();
            start.Clear();
            return result;
        }

        /// <summary>
        /// Merge Быстро (быстрый и не факт, что правильный)
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static IndexationElementInfo[] MergeFast(ref List<IndexationElementInfo> start, ref List<IndexationElementInfo> end)
        {
            for (int i = 0; i < start.Count; i++)
                start[i].EndLineNumber = end[i].LineNumber;
            return start.ToArray();
        }


    }
}
