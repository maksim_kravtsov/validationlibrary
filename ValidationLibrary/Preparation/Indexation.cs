﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ValidationLibrary.Objects;

namespace ValidationLibrary.Preparation
{
    /// <summary>
    /// Индексирование XML-документа.
    /// </summary>
    public class Indexation
    {
        /// <summary>
        /// Номер строки альтернативного корня
        /// </summary>
        public int AlternativeRootLineNumber = 0;
        /// <summary>
        /// Название альтернативного корня
        /// </summary>
        public string AlternativeRootName=null;
        /// <summary>
        /// Первый элемент альтернативного корня
        /// </summary>
        public string FirstElementInAlternativeRoot = null;
        /// <summary>
        /// Индексированные элементы XML-документа
        /// </summary>
        public List<IndexationElementInfo> IndexElements = new List<IndexationElementInfo>();
        public List<IndexationElementInfo> EndIndexElements = new List<IndexationElementInfo>();
        public List<string> names = new List<string>();
        /// <summary>
        /// Стак родительских позиций в документе. Нужен для процесса индексации
        /// </summary>
        private Stack<int> _parentNumberStack = new Stack<int>();
        /// <summary>
        /// Читатель Xml-документа
        /// </summary>
        private XmlTextReader _readerXMLDocument=null;
        /// <summary>
        /// Корень документа
        /// </summary>
        public string OriginalRootName = null;
        /// <summary>
        /// номер строки корня
        /// </summary>
        public int OriginalRootLineNumber;
        
        /// <summary>
        /// Конструктор индексации. Сразу запускает процесс индексирования
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="AlternativeRootName"></param>
        public Indexation(StreamReader stream, string AlternativeRootName, bool Start=true)
        {
            this.AlternativeRootName = AlternativeRootName;
            _readerXMLDocument = new XmlTextReader(stream);
            _readerXMLDocument.WhitespaceHandling = WhitespaceHandling.None;

            _parentNumberStack.Push(0);
            if (Start)
                readElements();
        }

        /// <summary>
        /// Процесс индексирования элементов XML-документа. Фиксирует позицию альтернативного корня
        /// </summary>
        public void readElements()
        {
            try
            {
                while (_readerXMLDocument.Read())
                {
                    switch (_readerXMLDocument.NodeType)
                    {
                        case XmlNodeType.Element:
                            var element = getElement(_parentNumberStack.Peek());
                            
                            if (OriginalRootName == null && _readerXMLDocument.Depth == 0)
                            {
                                OriginalRootName = element.Name;
                                OriginalRootLineNumber = element.LineNumber;
                            }
                            if (element.ParentElementLineNumber == AlternativeRootLineNumber && AlternativeRootLineNumber>0)
                                IndexElements.Add(element);
                            if (element.Name == AlternativeRootName) AlternativeRootLineNumber = element.LineNumber;
                            if (!_readerXMLDocument.IsEmptyElement) _parentNumberStack.Push(_readerXMLDocument.LineNumber);
                            break;
                        case XmlNodeType.EndElement:
                            _parentNumberStack.Pop();
                            var el = getElement(_parentNumberStack.Peek());
                            if (el.ParentElementLineNumber == AlternativeRootLineNumber && AlternativeRootLineNumber > 0)
                                EndIndexElements.Add(el);
                            break;
                    }
                }
            }
            finally
            {
                _readerXMLDocument.Dispose();
            }
        }

        /// <summary>
        /// выноска из readElements. Копирует значения из читателя в обьект elements.
        /// </summary>
        /// <param name="parentLineNumber"></param>
        /// <returns></returns>
        private IndexationElementInfo getElement(int parentLineNumber)
        {
            IndexationElementInfo this_element = new IndexationElementInfo();
            this_element.isEmpty = _readerXMLDocument.IsEmptyElement;
            this_element.LineNumber = _readerXMLDocument.LineNumber;
            this_element.Type = _readerXMLDocument.NodeType;
            this_element.Name = _readerXMLDocument.Name;
            this_element.ParentElementLineNumber = parentLineNumber;
            return this_element;
        }

    }
}
