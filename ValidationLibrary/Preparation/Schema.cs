﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using ValidationLibrary.Objects;
namespace ValidationLibrary.Preparation
{
    public class Schema
    {

        public void SetSchemaPath(SchemaParameters Parameters)
        {
            SetSchemaPath(Parameters.Path, Parameters.ElementName);
        }

        ///Схема загружается, делется схема блока. ЗАКОНЧЕН*

        //1. загрузка в память XSD. НУЖНО путь то XSD и имя
        /// <summary>
        /// имя альтернативного корня
        /// </summary>
        public static string _AlternativeRootElementName;

        /// <summary>
        /// Cхема валидации
        /// </summary>
        public XmlSchema XSDSchemaOriginal, XSDSchemaAlternative;

        /// <summary>
        /// Установка схемы документа, схема редактируется и готовится схема для блоков
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        public void SetSchemaPath(string path, string name)
        {
            int line = -1;
            _AlternativeRootElementName = name;
            string[] lines = File.ReadAllLines(path);
            XSDSchemaOriginal = new XmlSchemaSet().Add(null, path);
            for (int i = 0; i < lines.Length; i++)
                if (lines[i].IndexOf("name=") > -1)
                {
                    if (line == -1) line = i;
                    string[] temp = lines[i].Split(' ');
                    foreach (string str in temp)
                        if (str.IndexOf("name=") > -1)
                        {
                            string[] _name = str.Split('=');
                            if (_name[1].IndexOf(_AlternativeRootElementName) > -1)
                                lines[line] = lines[i];
                        }
                }

            XSDSchemaAlternative = new XmlSchemaSet().Add(null, XmlReader.Create(new StringReader(tostr(lines))));
        }

        private string tostr(string[] str)
        {
            string res = String.Empty;
            foreach (string s in str)
                res += s;
            return res;
        }
    }
}
